import React, { Component } from "react";
import { data_phone } from "./data_phone";
import Detailphone from "./Detailphone";
import ListPhone from "./ListPhone";

export default class Ex_Phone extends Component {
  state = {
    listPhone: data_phone,
    details: data_phone[0],
  };
  detailSP = (data) => {
    // console.log("data[0] la data ban dau", this.state.details);
    console.log("data moi'", data);
    this.setState({ details: data });
  };
  render() {
    return (
      <div className="container">
        <ListPhone handleDetail={this.detailSP} list={this.state.listPhone} />
        <Detailphone detail={this.state.details} />
      </div>
    );
  }
}
