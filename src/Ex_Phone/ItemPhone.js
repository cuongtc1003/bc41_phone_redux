import React, { Component } from "react";

export default class ItemPhone extends Component {
  render() {
    let { hinhAnh, giaBan, tenSP } = this.props.list1;
    return (
      <div className=" card-all col-sm-12 col-md-6 col-lg-4 p-5">
        <div className="card border-primary">
          <img className="card-img-top" src={hinhAnh} alt="" />
          <div className="card-body">
            <h4 className="card-title">{giaBan}</h4>
            <p className="card-text">{tenSP}</p>
            <button
              onClick={() => {
                this.props.handleDetail(this.props.list1);
              }}
              className="btn btn-success"
            >
              Xem Chi Tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
