import React, { Component } from "react";

export default class Detailphone extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.detail;
    return (
      <div className="product row">
        <div className="product-img col-5 col-lg-12">
          <img style={{ width: 500 }} src={hinhAnh} alt="" />
        </div>
        <div className="col-7 col-lg-12">
          <h2>{maSP}</h2>
          <h2>{tenSP}</h2>
          <h2>{manHinh}</h2>
          <h2>{heDieuHanh}</h2>
          <h2>{cameraTruoc}</h2>
          <h2>{cameraSau}</h2>
          <h2>{ram}</h2>
          <h2>{rom}</h2>
          <h2>{giaBan}$</h2>
        </div>
      </div>
    );
  }
}
