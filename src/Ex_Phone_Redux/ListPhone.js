import React, { Component } from "react";
import { connect } from "react-redux";
import ItemPhone from "./ItemPhone";

class ListPhone extends Component {
  renderListPhone = () => {
    return this.props.list.map((item, index) => {
      return <ItemPhone detail={this.props.detail} phone={item} key={index} />;
    });
  };
  render() {
    console.log(this.props.list);
    return <div className="row">{this.renderListPhone()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    list: state.phoneReducer.listPhone,
  };
};

export default connect(mapStateToProps, null)(ListPhone);
