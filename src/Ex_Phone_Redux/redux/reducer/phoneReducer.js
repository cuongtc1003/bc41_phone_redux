import { data_phone } from "../../data_phone";
let initialValue = {
  listPhone: data_phone,
  details: data_phone[0],
};
export const phoneReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "XEM_CHI_TIET": {
      return { ...state, details: action.payload };
    }
    default:
      return state;
  }
};
