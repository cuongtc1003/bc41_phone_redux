import React, { Component } from "react";
import { connect } from "react-redux";

class Detailphone extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.detail;
    return (
      <div className="product row">
        <div className="product-img col-6">
          <img style={{ width: 500 }} src={hinhAnh} alt="" />
        </div>
        <div className="col-6">
          <h2>{maSP}</h2>
          <h2>{tenSP}</h2>
          <h2>{manHinh}</h2>
          <h2>{heDieuHanh}</h2>
          <h2>{cameraTruoc}</h2>
          <h2>{cameraSau}</h2>
          <h2>{ram}</h2>
          <h2>{rom}</h2>
          <h2>{giaBan}$</h2>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.phoneReducer.details,
  };
};

export default connect(mapStateToProps, null)(Detailphone);
